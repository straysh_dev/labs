1. 测试程序是否正确
```
gcc -o hello hello.c hello_test.c
./hello
输出：hello gopher
```

2. 静态链接方式
```
gcc -v hello.c hello_test.c -o hello1
```

3. 动态链接需要先构建我们自己的 so 文件
```
gcc -shared -fpic -o hello.so hello.c
```

然后编译hello2
```
gcc -v hello_test.c -o hello2 ./hello.so
```
package main

// 入门例子
// 引用的C头文件需要在注释中声明
// 紧接着注释有import "C",且这一行与注释之间不能有空格

/*
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

void hellogopher(char* s) {
    printf("%s:hello gopher.\n", s);
}
*/
import "C"
import (
	"fmt"
	"unsafe"
)

func main() {
	//使用C.CString创建字符串需要手动释放
	name := C.CString("straysh")
	C.hellogopher(name)
	C.free(unsafe.Pointer(name))

	fmt.Println("call C.Sleep(3) for 3 seconds")
	C.sleep(3)
}

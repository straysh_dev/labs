1. 生成可重定向目标文件hello.o
```
gcc -c hello.c
```

2. 反汇编hello.o得到hello.o.txt
```
objdump -d hello.o > hello.o.txt
```

3. 生成可执行文件hello
```
gcc -o hello hello.o hello_main.c 
```

4. 反汇编得到hello.txt
```
objdump -d hello > hello.txt
```

对比hello.o.txt与hello.txt
hello.o.txt
```
0000000000000000 <hello>:
   0:	55                   	push   %rbp
   1:	48 89 e5             	mov    %rsp,%rbp
   4:	bf 00 00 00 00       	mov    $0x0,%edi
   9:	b8 00 00 00 00       	mov    $0x0,%eax
   e:	e8 00 00 00 00       	callq  13 <hello+0x13>
  13:	90                   	nop
  14:	5d                   	pop    %rbp
  15:	c3                   	retq  
```

hello.txt
```
0000000000400526 <hello>:
  400526:	55                   	push   %rbp
  400527:	48 89 e5             	mov    %rsp,%rbp
  40052a:	bf d4 05 40 00       	mov    $0x4005d4,%edi
  40052f:	b8 00 00 00 00       	mov    $0x0,%eax
  400534:	e8 c7 fe ff ff       	callq  400400 <printf@plt>
  400539:	90                   	nop
  40053a:	5d                   	pop    %rbp
  40053b:	c3                   	retq 
```

节地址映射到了可执行的内存地址。
package main

/*
#cgo CFLAGS: -I./src/
#include <stdlib.h>
#include "include/sign_elf.h"
#include "include/cert.h"
#include "src/util/fio.c"
#include "src/util/cert.c"
#include "src/util/elf_util.c"
#include "src/util/sign_elf.c"
#include "src/util/elf_section_op.c"
#include "src/util/elf_section_op_util.c"
#include "src/util/parse_cert_oid.c"
#include "src/util/sign_elf_main.c"
*/
import "C"


func main(){
	debFile := "/app/Golang/UnionTech/caotinghan/labs/uosca/test/data/test001.deb"
	keyFile := "/app/Golang/UnionTech/caotinghan/labs/uosca/test/certs/intermediate.key"
	certFile := "/app/Golang/UnionTech/caotinghan/labs/uosca/test/certs/intermediate.crt"

	Cdeb  := C.CString(debFile)
	Ckey  := C.CString(keyFile)
	Ccert := C.CString(certFile)
	defer C.free(Cdeb)
	defer C.free(Ckey)
	defer C.free(Ccert)

	C.sign_elf(Cdeb, Ckey, Ccert)
}
package test

import (
	"crypto"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/x509"
	"encoding/binary"
	"encoding/hex"
	"encoding/pem"
	"errors"
	"fmt"
	"io"
	"strconv"
	"strings"
	"testing"

	"github.com/blakesmith/ar"
)

var (
	ErrVerify = errors.New("signature verify failed")
)

func TestDebSignV1(t *testing.T) {
	x := int64(10)
	fmt.Println(strconv.FormatInt(x, 8))
}

func debVerity(in io.Reader) (*x509.Certificate, error) {
	r := ar.NewReader(in)
	var controlhash, datahash []byte
	for {
		header, err := r.Next()
		if err != nil {
			if err == io.EOF {
				break
			}
			return nil, fmt.Errorf("ar next: %w", err)
		}
		switch {
		case strings.HasPrefix(header.Name, "control.tar"):
			controlhash, err = sha256Reader(r)
			if err != nil {
				return nil, fmt.Errorf("sha256 control file: %w", err)
			}
		case strings.HasPrefix(header.Name, "data.tar"):
			datahash, err = sha256Reader(r)
			if err != nil {
				return nil, fmt.Errorf("sha256 data file: %w", err)
			}
		case strings.HasPrefix(header.Name, "sign.tar"):
			var sign struct {
				Version  int32
				CertLen  int32
				CertData [4096]byte
				SignLen  int32
				SignData [512]byte
			}
			err = binary.Read(r, binary.LittleEndian, &sign)
			if err != nil {
				return nil, fmt.Errorf("unmarshal sign file: %w", err)
			}
			pd, _ := pem.Decode(sign.CertData[:sign.CertLen])
			cer, err := x509.ParseCertificate(pd.Bytes)
			if err != nil {
				return nil, fmt.Errorf("parse cert: %w", err)
			}
			publickey := cer.PublicKey.(*rsa.PublicKey)
			mergeHash := sha256.Sum256([]byte(strings.Join([]string{hex.EncodeToString(controlhash), hex.EncodeToString(datahash)}, "-")))
			hash := sha256.Sum256(append([]byte(hex.EncodeToString(mergeHash[:])), 0))
			err = rsa.VerifyPKCS1v15(publickey, crypto.SHA256, hash[:], sign.SignData[:sign.SignLen])
			if err != nil {
				return nil, fmt.Errorf("verify pkcs: %s :%w", err, ErrVerify)
			}
			return cer, nil
		}
	}
	return nil, fmt.Errorf("not found")
}

func sha256Reader(r io.Reader) ([]byte, error) {
	h := sha256.New()
	_, err := io.Copy(h, r)
	if err != nil {
		return nil, err
	}
	return h.Sum(nil), nil
}

package test

import (
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"io/ioutil"
	"os"
	"testing"

	"github.com/joho/godotenv"
)

func setUp(t *testing.T) {
	if err := godotenv.Load("../.env"); err != nil {
		t.Fatalf("could not load .env: %v", err)
	}
}

func TestStandardRsa(t *testing.T) {
	setUp(t)

	rsaPrivateKeyPassword := os.Getenv("rsaPrivateKeyPassword")
	// 从文件读取私钥
	//privk := "/tools/certificates/out/1.key"
	privk := "/test/certs/intermediate.key"
	crtk := "/test/certs/intermediate.crt"

	priv, err := ioutil.ReadFile(os.Getenv("APP_ROOT") + privk)
	if err != nil {
		t.Fatalf("could read privkey %s: %v", os.Getenv("APP_ROOT")+privk, err)
	}
	t.Logf("priv:%s", priv)

	privPem, _ := pem.Decode(priv)
	var privPemBytes []byte
	if privPem.Type != "RSA PRIVATE KEY" {
		t.Fatalf("RSA private key is of the wrong type:%s", privPem.Type)
	}

	if rsaPrivateKeyPassword != "" {
		privPemBytes, err = x509.DecryptPEMBlock(privPem, []byte(rsaPrivateKeyPassword))
		if err != nil {
			t.Fatalf("could not decrypt privPem from password `%s`: %v", rsaPrivateKeyPassword, err)
		}
	} else {
		privPemBytes = privPem.Bytes
	}

	var parsedKey interface{}
	if parsedKey, err = x509.ParsePKCS1PrivateKey(privPemBytes); err != nil {
		if parsedKey, err = x509.ParsePKCS8PrivateKey(privPemBytes); err != nil {
			t.Fatalf("could not parse RSA privatekey: %v", err)
		}
	}

	var privateKey *rsa.PrivateKey
	var ok bool
	privateKey, ok = parsedKey.(*rsa.PrivateKey)
	if !ok {
		t.Fatalf("could not convert parsedKey to *rsa.PrivateKey")
	}
	_ = privateKey

	//pub,err := ioutil.ReadFile(os.Getenv("APP_ROOT") + pubk)
	//if err!=nil {
	//    t.Fatalf("could read pubkey %s: %v", os.Getenv("APP_ROOT") + pubk, err)
	//}
	//t.Logf("pub:%s", pub)
	//
	//pubPem,_ := pem.Decode(pub)
	//if pubPem==nil {
	//    t.Fatal("Use `ssh-keygen -f id_rsa.pub -e -m pem > id_rsa.pem` to generate the pem encoding of your RSA public key")
	//}
	//if pubPem.Type != "RSA PUBLIC KEY" {
	//    t.Fatalf("RSA public key is of the wrong type:%s", pubPem.Type)
	//}
	//
	//if parsedKey,err = x509.ParsePKIXPublicKey(pubPem.Bytes);err!=nil {
	//    t.Fatalf("could not parse publickey: %v", err)
	//}
	//
	//var pubKey *rsa.PublicKey
	//if pubKey,ok = parsedKey.(*rsa.PublicKey); !ok {
	//    t.Fatalf("could not convert parseKey to *rsa.PublicKey")
	//}
	//privateKey.PublicKey = *pubKey

	crt, err := ioutil.ReadFile(os.Getenv("APP_ROOT") + crtk)
	if err != nil {
		t.Fatalf("could read pubkey %s: %v", os.Getenv("APP_ROOT")+crtk, err)
	}
	t.Logf("crt:%s", crt)
	block, _ := pem.Decode(crt)
	var cert *x509.Certificate
	cert, err = x509.ParseCertificate(block.Bytes)
	if err != nil {
		t.Fatalf("could parse certificte `%s`: %v", crtk, err)
	}
	pubKey := cert.PublicKey.(*rsa.PublicKey)
	privateKey.PublicKey = *pubKey

	t.Logf("privateKey:%v", privateKey)
}

#!/bin/bash -x

# Only for test purpose, from: https://stackoverflow.com/a/40530391
# 该脚本在目录`root-ca`创建了根证书，在目录`intermediate`目录下创建了中间人证书，最后颁发了三个使用中间人签发的证书到`out`目录。

set -e

mkdir certificates
cd certificates

for C in $(echo root-ca intermediate); do

  mkdir "$C"
  cd "$C"
  mkdir certs crl newcerts private
  cd ..

  echo 1000 > "$C"/serial
  touch "$C"/index.txt "$C"/index.txt.attr

# shellcheck disable=SC2016
  echo '
[ ca ]
default_ca = CA_default
[ CA_default ]
dir                = '"$C"'                      # Where everything is kept
certs              = $dir/certs               # Where the issued certs are kept
crl_dir            = $dir/crl                 # Where the issued crl are kept
database           = $dir/index.txt           # database index file.
new_certs_dir      = $dir/newcerts            # default place for new certs.
certificate        = $dir/cacert.pem          # The CA certificate
serial             = $dir/serial              # The current serial number
crl                = $dir/crl.pem             # The current CRL
private_key        = $dir/private/ca.key.pem  # The private key
RANDFILE           = $dir/.rnd                # private random number file
nameopt            = default_ca
certopt            = default_ca
policy             = policy_match
default_days       = 365
default_crl_days   = 365
default_md         = sha256

[ policy_match ]
countryName            = optional
stateOrProvinceName    = optional
organizationName       = optional
organizationalUnitName = optional
commonName             = supplied
emailAddress           = optional

[req]
req_extensions = v3_req
distinguished_name = req_distinguished_name

[req_distinguished_name]

[v3_req]
basicConstraints = CA:TRUE
' > "$C"/openssl.conf
done

# 生成私钥
openssl genrsa -out root-ca/private/ca.key 2048
# 生成根证书
openssl req -config root-ca/openssl.conf -new -x509 -days 3650 -key root-ca/private/ca.key -sha256 -extensions v3_req -out root-ca/certs/ca.crt -subj '/CN=Root-ca'

# 生成私钥
openssl genrsa -out intermediate/private/intermediate.key 2048
# 生成证书请求文件intermediate/certs/intermediate.csr
openssl req -config intermediate/openssl.conf -sha256 -new -key intermediate/private/intermediate.key -out intermediate/certs/intermediate.csr -subj '/CN=Interm.'
# 使用根证书给intermediate颁发证书
openssl ca -batch -config root-ca/openssl.conf -keyfile root-ca/private/ca.key -cert root-ca/certs/ca.crt -extensions v3_req -notext -md sha256 -in intermediate/certs/intermediate.csr -out intermediate/certs/intermediate.crt

mkdir out

#for I in $(seq 1 3) ; do
#  # 创建私钥并生成证书请求文件
#  openssl req -new -keyout out/$I.key -out out/$I.request -days 365 -nodes -subj "/CN=$I.example.com" -newkey rsa:2048
#  # 使用中间人证书颁发个人证书
#  openssl ca -batch -config root-ca/openssl.conf -keyfile intermediate/private/intermediate.key -cert intermediate/certs/intermediate.crt -out out/$I.crt -infiles out/$I.request
#done

for I in $(seq 1 1) ; do
  openssl genrsa -out out/$I.key 2048
  # 创建私钥并生成证书请求文件
  openssl req -new -key out/$I.key -out out/$I.request -days 365 -nodes -subj "/CN=$I.example.com"
#  # 使用中间人证书颁发个人证书
  openssl ca -batch -config root-ca/openssl.conf -keyfile intermediate/private/intermediate.key -cert intermediate/certs/intermediate.crt -out out/$I.crt -infiles out/$I.request
done

module gitlab.deepin.io/caotighan/labs

go 1.13

require (
	github.com/blakesmith/ar v0.0.0-20190502131153-809d4375e1fb
	github.com/joho/godotenv v1.3.0
)
